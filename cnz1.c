#include <stdio.h>
#include <stdbool.h>

int main(){
    char fld[3][3];
    int i, j, x, y, mv=0;
    bool cm = true;
    char player = 'X';
    char winner = '0';

    // initialization of gamefield
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            fld[i][j] = '-';

    // intro
    

    // game cycle
    do {
        // define player
        player = (player == 'O') ? 'X': 'O';

        // print gamefield
        system("clear");
        printf("ИГРА 'КРЕСТИКИ-НОЛИКИ'\n");
        printf("ХОД ВВОДИТЕ В ФОРМАТЕ: ГОРИЗОНТАЛЬ [ПРОБЕЛ] ВЕРТИКАЛЬ [ВВОД]\n");
        printf("ДИАПАЗОН ЗНАЧЕНИЙ ПО ГОРИЗОНТАЛИ/ВЕРТИКАЛИ ОТ 1 ДО 3\n");
        printf("\n");
        for (i = 0; i < 3; i++){
            for (j = 0; j < 3; j++){
                printf("%c ", fld[i][j]);
                // check if winner Xline
                if (fld[0][j] != '-' && fld[0][j] == fld[1][j] && fld[1][j] == fld[2][j])
                    winner = fld[0][j];
            }
            // check if winner Yline
            if (fld[i][0] != '-' && fld[i][0] == fld[i][1] && fld[i][1] == fld[i][2])
                winner = fld[i][0];

            printf("\n");
        }
        // check if winner diags
        if (fld[0][0] != '-' && fld[0][0] == fld[1][1] && fld[1][1] == fld[2][2])
                winner = fld[0][0];
        if (fld[0][2] != '-' && fld[0][2] == fld[1][1] && fld[1][1] == fld[2][0])
                winner = fld[0][2];

        if (winner != '0') break; // end game by winning

        do {
            printf("\nВВЕДИТЕ ХОД [%c]: ", player);
            scanf("%d %d", &y, &x);
            if (x == 0 && y == 0) break; // exit from game
            if ( (y < 1) || (y > 3) || (x < 1) || (x > 3) ){
                printf("\nОШИБКА: НЕДОПУСТИМОЕ ЗНАЧЕНИЕ");
                cm = false;
            }
        } while (cm == false);
        if (x == 0 && y == 0) break; // exit from game

        fld[y-1][x-1] = player;
        mv++;
    } while (mv < 9);

    if (winner != '0')
        printf("\nПОБЕДИЛИ %c-КИ!!! \n", winner);
    else
        printf("\nПОБЕДИЛА ДРУЖБА!!!\n");

    return 0;
}
